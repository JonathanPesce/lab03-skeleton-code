package course.labs.activitylab;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class ActivityOne extends Activity {

	// string for logcat documentation
	private final static String TAG = "Lab-ActivityOne";

	// lifecycle counts
	private int countOnCreate, countOnResume, countOnStart, countOnPause, countOnDestroy, countOnStop, countOnRestart;
	private TextView onCreateView, onResumeView, onStartView, onPauseView, onDestroyView, onStopView, onRestartView;
	//TODO:  increment the variables' values when their corresponding lifecycle methods get called.

	private void updateGUI() {
		onCreateView.setText(getResources().getString(R.string.onCreate) + countOnCreate);
		onResumeView.setText(getResources().getString(R.string.onResume) + countOnResume);
		onStartView.setText(getResources().getString(R.string.onStart) + countOnStart);
		onPauseView.setText(getResources().getString(R.string.onPause) + countOnPause);
		onDestroyView.setText(getResources().getString(R.string.onDestroy) + countOnDestroy);
		onStopView.setText(getResources().getString(R.string.onStop) + countOnStop);
		onRestartView.setText(getResources().getString(R.string.onRestart) + countOnRestart);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_one);

		//Log cat print out
		Log.i(TAG, "onCreate called");

		//TODO: update the appropriate count variable & update the view
		countOnCreate++;

		onCreateView = (TextView) findViewById(R.id.create);
		onDestroyView = (TextView) findViewById(R.id.destroy);
		onResumeView = (TextView) findViewById(R.id.resume);
		onRestartView = (TextView) findViewById(R.id.restart);
		onStartView = (TextView) findViewById(R.id.start);
		onStopView = (TextView) findViewById(R.id.stop);
		onPauseView = (TextView) findViewById(R.id.pause);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_one, menu);
		return true;
	}

	// lifecycle callback overrides

	@Override
	public void onStart(){
		super.onStart();

		//Log cat print out
		Log.i(TAG, "onStart called");
		//TODO:  update the appropriate count variable & update the view
		countOnStart++;
	}

	// TODO: implement 5 missing lifecycle callback methods
	@Override
	public void onResume() {
		super.onResume();

		//Log cat print out
		Log.i(TAG, "onResume called");
		//TODO:  update the appropriate count variable & update the view
		countOnResume++;
        updateGUI();
	}

	@Override
	public void onPause() {
		super.onPause();

		//Log cat print out
		Log.i(TAG, "onPause called");
		//TODO:  update the appropriate count variable & update the view
		countOnPause++;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		//Log cat print out
		Log.i(TAG, "onDestroy called");
		//TODO:  update the appropriate count variable & update the view
		countOnDestroy++;
	}

	@Override
	public void onStop() {
		super.onStop();

		//Log cat print out
		Log.i(TAG, "onStop called");
		//TODO:  update the appropriate count variable & update the view
		countOnStop++;
	}

	@Override
	public void onRestart() {
		super.onRestart();

		//Log cat print out
		Log.i(TAG, "onRestart called");
		//TODO:  update the appropriate count variable & update the view
		countOnRestart++;
	}

	// Note:  if you want to use a resource as a string you must do the following
	//  getResources().getString(R.string.stringname)   returns a String.

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState){
		//TODO:  save state information with a collection of key-value pairs & save all  count variables
	}


	public void launchActivityTwo(View view) {
		startActivity(new Intent(this, ActivityTwo.class));
	}
}
